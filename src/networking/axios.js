// ---------- DEPENDENCIES ---------- //

import Axios from 'axios';

// ---------- MAIN ---------- //

const base = process.env.REACT_APP_URL_BASE;

const instance = Axios.create({
  baseURL: `${base}/`,
});

export default instance;
