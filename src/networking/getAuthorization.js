import { FirebaseUtils } from '../firebase';

const withIDToken = () =>
  new Promise((resolve, reject) => {
    if (!FirebaseUtils.Auth.currentUser) {
      return reject(new Error('User is not logged in.'));
    }
    return FirebaseUtils.Auth.currentUser.getIdToken(true)
      .then((authorization) => {
        if (authorization === undefined) {
          console.error('Generated access token undefined');
          return reject(new Error('There was a problem, please try again later.'));
        }
        return resolve(authorization);
      }).catch((error) => {
        console.error(error);
        return reject(new Error('There was a problem, please try again later.'));
      });
  });

export default withIDToken;
