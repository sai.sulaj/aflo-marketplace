import qs from 'qs';
import Axios from './axios';
import getAuthorization from './getAuthorization';

export async function doGetBlogPosts(filter) {
  let url = '/blogposts';

  if (filter !== undefined) {
    url = `${url}?${qs.stringify(filter, { allowDots: true })}`;
  }
  const response = await Axios.get(url);

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doGetBlogPost(postId) {
  const response = await Axios.get(`/blogposts/${postId}`);

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doGetBlogPostByTitle(title) {
  if (title === undefined) {
    throw new Error('Title cannot be empty');
  }

  const query = qs.stringify({
    title_eq: title,
  });
  const response = await Axios.get(`/blogposts?${query}`);

  if (response && Array.isArray(response.data) && response.data.length > 0) {
    return response.data[0];
  }

  throw new Error('Empty response');
}

export async function doGetTag(tagId) {
  const response = await Axios.get(`/tags/${tagId}`);

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doGetFeaturedBlogPosts() {
  const response = await Axios.get('/blogposts?_limit=2&featured=true');

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doGetRecentBlogPosts(page = 0, numPosts = 6) {
  const response = await Axios.get(`/blogposts?&_sort=created_at:DESC&_start=${page}&_limit=${numPosts}`);

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doGetNumberOfPosts() {
  const response = await Axios.get('/blogposts/count');

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response.');
}

export async function doGetTags() {
  const response = await Axios.get('/tags');

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doGetComments(blogPostId, from, limit) {
  const response = await Axios.get(`/comments?blogPostId=${blogPostId}&_start=${from}&_limit=${limit}&_sort=created_at:DESC`);

  if (response && response.data) {
    return response.data;
  }

  throw new Error('Empty response');
}

export async function doSubmitComment(blogPostId, displayName, message) {
  const authorization = await getAuthorization();
  await Axios.post('/comments', {
    blogPostId,
    message,
    displayName,
  }, {
    headers: {
      Authorization: authorization,
    },
  });
}
