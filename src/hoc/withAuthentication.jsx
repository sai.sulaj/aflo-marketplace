// ------------- DEPENDENCIES -------------- //

import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withRouter } from 'react-router-dom';

// ------------- LOCAL DEPENDENCIES -------------- //

// Constants.
import {
  ActionTypes,
} from '../constants';

// Components.

import {
  FirebaseUtils,
} from '../firebase';

// ------------- MAIN -------------- //

/**
 * Component wrapper that providers authUser object when available to
 * component.
 *
 * @param {React.Component} Component -> Arbitrary component that requires
 *                                       authentication to access.
 *
 * @return {React.Component} Component -> Passed component wrapped with
 *                                        helper.
 */
const withAuthentication = (Component) => {
  class WithAuthentication extends React.Component {
    componentDidMount = () => {
      const {
        onSetAuthUser,
      } = this.props;

      FirebaseUtils.Auth.onAuthStateChanged((authUser) => {
        if (authUser) {
          onSetAuthUser(authUser);
        } else {
          onSetAuthUser(null);
        }
      });
    }

    render() {
      return <Component />;
    }
  }

  const mapDispatchToProps = (dispatch) => ({
    onSetAuthUser: (authUser) =>
      dispatch({
        type: ActionTypes.AUTH_USER_SET,
        authUser,
      }),
  });

  return compose(
    withRouter,
    connect(
      null,
      mapDispatchToProps,
    ),
  )(WithAuthentication);
};

export default withAuthentication;
