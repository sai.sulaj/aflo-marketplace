import {
  useRef,
  useEffect,
} from 'react';

export default function useOutsideClickAlert(alert) {
  const ref = useRef(null);

  function handleClick(event) {
    if (alert && ref.current && !ref.current.contains(event.target)) {
      alert();
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClick);
    return () => {
      document.removeEventListener('mousedown', handleClick);
    };
  }, []);

  return ref;
}
