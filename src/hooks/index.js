import useOutsideClickAlert from './useOutsideClickAlert';

export {
  useOutsideClickAlert,
};
