// ------------- DEPENDENCIES ------------- //

import { Auth } from './firebase';

// ------------- MAIN ------------- //

/**
 * Creates a Firebase user.
 *
 * @type {function(!string, !string, !string)}
 *
 * @public
 *
 * @param {string} email -> User email string.
 * @param {string} password -> User password string.
 * @param {string} displayName -> User's display name.
 */
export const doCreateUserWithEmailAndPassword = async (email, password, displayName) => {
  const result = await Auth.createUserWithEmailAndPassword(email, password);
  await result.user.updateProfile({
    displayName,
  });
};

/**
 * Signs in a Firebase user.
 *
 * @type {function(!string, !string)}
 *
 * @public
 *
 * @param {string} email -> User email string.
 * @param {string} password -> User password string.
 */
export const doSignInWithEmailAndPassword = (email, password) =>
  Auth.signInWithEmailAndPassword(email, password);

/**
 * Signs out a Firebase user.
 *
 * @type {function()}
 *
 * @public
 */
export const doSignOut = () => Auth.signOut();

/**
 * Sends user a password reset email.
 *
 * @type {function(!string)}
 *
 * @public
 *
 * @param {string} email -> User email string.
 */
export const doPasswordReset = (email) => Auth.sendPasswordResetEmail(email);

/**
 * Changes Firebase user's password.
 *
 * @type {function(!string)}
 *
 * @public
 *
 * @param {string} password -> User new password string.
 */
export const doPasswordUpdate = (password) => Auth.currentUser.updatePassword(password);
