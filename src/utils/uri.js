export function encodeBlogPostTitle(title) {
  return encodeURIComponent(title.replace(/ /g, '_'));
}

export function decodeBlogPostTitle(title) {
  return decodeURIComponent(title.replace(/_/g, ' '));
}
