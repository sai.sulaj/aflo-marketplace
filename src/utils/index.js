import safeAccess from './safeAccess';
import * as URIUtils from './uri';

export {
  safeAccess,
  URIUtils,
};
