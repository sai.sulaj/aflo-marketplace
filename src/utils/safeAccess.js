export default function safeAccess(path, object) {
  // eslint-disable-next-line no-confusing-arrow
  return path.reduce((acc, curr) => (acc && acc[curr]) ? acc[curr] : null, object);
}
