export const LANDING = '/';
export const ABOUT = '/about';
export const CONTACT = '/contact';
export const BLOG_POST = '/blog-post';
export const FILTERED_POSTS = '/filtered-posts';
