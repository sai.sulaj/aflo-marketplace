// --------------- DEPENDENCIES -------------- //

import React, {
  useEffect,
  useState,
} from 'react';
import qs from 'qs';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Components.
import Navbar from '../../components/Navbar';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import WithLoader from '../../components/WithLoader';
import { RecentPost } from '../../components/RecentPosts';

// Networking.
import { DbNetUtils } from '../../networking';

// --------------- MAIN --------------- //

function FilteredPosts(props) {
  const classes = useStyles(props);
  const { location } = props;
  const [tag, setTag] = useState({ image: {} });
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  async function getTag() {
    const { search = '?' } = location;
    const query = qs.parse(search.slice(1));

    if (query.tag) {
      const newPost = await DbNetUtils.doGetTag(query.tag);
      setTag(newPost);
    } else {
      console.error('Tag ID not specified');
    }
  }

  async function getPosts() {
    const { id } = tag;

    setLoading(true);
    try {
      const newPosts1 = await DbNetUtils.doGetBlogPosts({ tag1: { id } });
      const newPosts2 = await DbNetUtils.doGetBlogPosts({ tag2: { id } });
      setPosts([...newPosts1, ...newPosts2]);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getTag();
  }, []);
  useEffect(() => {
    if (tag && tag.id !== undefined) {
      getPosts();
    }
  }, [tag && tag.id]);

  return (
    <div className={classes.featuredPosts}>
      <Navbar />
      <Header
        imageUrl={`${process.env.REACT_APP_URL_BASE}${tag.image.url}`}
        header={tag.label}
      />
      <div className={classes.posts}>
        <WithLoader loading={loading}>
          {posts.map((post) => (
            <RecentPost key={post.id} {...post} />
          ))}
        </WithLoader>
      </div>
      <Footer />
    </div>
  );
}

export default FilteredPosts;
