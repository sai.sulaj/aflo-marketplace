// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  featuredPosts: {

  },
  posts: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: '62px 0',
    '& > *': {
      maxWidth: '34%',
    },
  },
});
