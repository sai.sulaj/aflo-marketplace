// --------------- DEPENDENCIES -------------- //

import React from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Components.
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';

// --------------- MAIN --------------- //

function Contact(props) {
  const classes = useStyles(props);

  return (
    <div className={classes.contact}>
      <Navbar />
      <h1 className={classes.header}>
        Let&apos;s Connect
      </h1>
      <div className={classes.content}>
        <div className={classes.contentInner}>
          <div className={classes.column}>
            <h2 className={classes.textHeader}>
              Connect with Us
            </h2>
            <p className={classes.textBody}>
              {/* eslint-disable-next-line */}
              We want to build the right community for you. Feel free to contact us with any feedback, partnership opportunities, or whatever really.
              <br />
              <br />
              We read all correspondence because we care. We’d love to hear from you.
            </p>
            <a
              className={classes.mailButton}
              href="mailto:info@aflo.io"
              target="_blank"
              rel="noopener noreferrer"
            >
              Contact Us
            </a>
          </div>
        </div>
      </div>
      <Footer backgroundColor="#E8E8E844" />
    </div>
  );
}

export default Contact;
