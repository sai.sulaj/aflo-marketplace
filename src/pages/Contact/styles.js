// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  contact: {
    backgroundColor: `${Colors.LIGHT_GREY}33`,
  },
  header: {
    width: '100%',
    paddingTop: 130,
    paddingBottom: 30,
    textAlign: 'center',
    margin: 0,
    fontFamily: "'Oxygen', sans-serif !important",
    fontSize: 40,
    color: Colors.DARK_GREY,
  },
  content: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '0 5%',
  },
  contentInner: {
    width: 980,
    padding: '64px 0',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'flex-start',
    boxSizing: 'border-box',
    '& > :first-child': {
      paddingRight: 20,
      '@media only screen and (max-width: 600px)': {
        padding: 0,
      },
    },
    '& > :not(:first-child)': {
      paddingLeft: 20,
      '@media only screen and (max-width: 600px)': {
        padding: 0,
      },
    },
  },
  column: {
    flex: '1 0 44%',
    '@media only screen and (max-width: 600px)': {
      flex: '1 0 100%',
    },
  },
  textHeader: {
    fontSize: 18,
    marginBottom: 20,
  },
  textBody: {
    fontSize: 15,
    lineHeight: 1.5,
    marginBottom: 30,
  },
  socialLink: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    paddingBottom: 20,
    width: '100%',
    textDecoration: 'none',
    color: Colors.DARK_GREY,
  },
  socialLinkIcon: {
    height: 14,
    width: 14,
    marginRight: 10,
  },
  socialLinkLabel: {
    fontSize: 14,
    flex: 1,
    cursor: 'pointer',
    transition: 'color 100ms linear',
    '&:hover': {
      color: Colors.BROWN,
    },
  },
  mailButton: {
    backgroundColor: Colors.BLUE,
    color: Colors.WHITE,
    height: 41,
    padding: '12px 31px',
    fontSize: 14,
    bordeR: 'none',
    borderRadius: 3,
    textDecoration: 'none',
  },
});
