// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
  useEffect,
} from 'react';
import Moment from 'moment';
import qs from 'qs';
import Markdown from 'react-markdown/with-html';
import { Helmet } from 'react-helmet';
import './custom.css';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Components.
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';
import AuthorInfo from '../../components/AuthorInfo';
import Comments from '../../components/Comments';
import RecentPosts from '../../components/RecentPosts';

// Networking.
import { DbNetUtils } from '../../networking';

// Utils.
import { URIUtils } from '../../utils';

// --------------- MAIN --------------- //

function BlogPostHeader(props) {
  const classes = useStyles(props);
  const {
    title,
    created_at,
    tag1,
    tag2,
  } = props;

  const safeTag1 = tag1 || {};
  const safeTag2 = tag2 || {};

  return (
    <div className={classes.header}>
      <h1 className={classes.title}>
        {title}
      </h1>
      <div className={classes.headerRow}>
        <span className={classes.created_at}>
          {created_at && Moment(created_at).format('MMMM D, YYYY')}
        </span>
        <span
          className={classes.category}
          style={{
            color: safeTag1.color,
          }}
        >
          {safeTag1.label}
        </span>
        <span
          className={classes.category}
          style={{
            color: safeTag2.color,
          }}
        >
          {safeTag2.label}
        </span>
      </div>
      <div className={classes.divider} />
    </div>
  );
}

function BlogPost(props) {
  const classes = useStyles(props);
  const [post, setPost] = useState({});
  const { location } = props;
  const {
    id,
    title,
    created_at,
    tag1,
    tag2,
    body = '',
    summary = '',
    author = {},
    image = { url: '' },
  } = post;

  async function getPost() {
    const { search = '?' } = location;
    const query = qs.parse(search.slice(1));

    if (query.id) {
      const newPost = await DbNetUtils.doGetBlogPost(query.id);
      setPost(newPost);
    } else if (query.title) {
      let parsed = query.title;
      if (parsed.indexOf(' ') === -1) {
        parsed = URIUtils.decodeBlogPostTitle(parsed);
      }
      const newPost = await DbNetUtils.doGetBlogPostByTitle(parsed);
      setPost(newPost);
    } else {
      console.error('Post ID not specified');
    }
  }

  useEffect(() => {
    getPost();
    window.scrollTo(0, 0);
  }, [location]);

  const imageUrl = `${process.env.REACT_APP_URL_BASE}${image.url}`;

  return (
    <div className={classes.blogPost}>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={summary} />

        <meta itemProp="name" content={title} />
        <meta itemProp="description" content={summary} />
        <meta itemProp="image" content={imageUrl} />

        <meta property="og:url" content={window.location.href} />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={summary} />
        <meta property="og:image" content={imageUrl} />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content={title} />
        <meta name="twitter:description" content={summary} />
        <meta name="twitter:image" content={imageUrl} />
      </Helmet>
      <Navbar />
      <div className={classes.content}>
        <div className={classes.contentInner}>
          <BlogPostHeader
            title={title}
            created_at={created_at}
            tag1={tag1}
            tag2={tag2}
          />
          <p className={classes.textBody}>
            <Markdown
              source={body}
              escapeHtml={false}
            />
          </p>
          <div className={classes.divider} />
          <AuthorInfo author={author} />
          <Comments blogPostId={id} />
        </div>
      </div>
      <RecentPosts backgroundColor="white" />
      <Footer />
    </div>
  );
}

export default BlogPost;
