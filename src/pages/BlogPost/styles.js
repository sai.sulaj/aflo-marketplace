// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  blogPost: {},
  content: {
    width: '100vw',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 62,
    paddingTop: 80,
  },
  contentInner: {
    maxWidth: 980,
    width: '100%',
  },
  divider: {
    borderBottom: `1px solid ${Colors.MED_GREY}33`,
    margin: '35px 0',
    width: '100%',
  },
  header: {
    width: '100%',
  },
  title: {
    marginTop: 62,
    marginBottom: 15,
    fontSize: 38,
    width: '100%',
    textAlign: 'center',
  },
  headerRow: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'center',
    '& > :not(:last-child)': {
      marginRight: 20,
    },
  },
  date: {
    fontSize: 14,
    color: Colors.MED_GREY,
  },
  category: {
    textTransform: 'uppercase',
    fontSize: 14,
  },
  textBody: {
    padding: '10px 71px',
    '@media only screen and (max-width: 600px)': {
      padding: '10px 21px',
    },
  },
});
