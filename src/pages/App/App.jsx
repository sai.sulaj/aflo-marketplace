// -------------- DEPENDENCIES -------------- //

import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { SnackbarProvider } from 'notistack';
import { Helmet } from 'react-helmet';

// -------------- LOCAL DEPENDENCIES -------------- //

import { Routes } from '../../constants';
import './App.css';

import Landing from '../Landing';
import About from '../About';
import Contact from '../Contact';
import BlogPost from '../BlogPost';
import FilteredPosts from '../FilteredPosts';
import AuthModal from '../../components/AuthModal';

import { withAuthentication } from '../../hoc';

// -------------- MAIN -------------- //

function AuthenticatedRoutesComponent() {
  return (
    <>
      <Helmet>
        <title>Aflo.io | Get Marketing and Sales Strategies Every Week</title>
        <meta name="description" content="Users to AFLO.io: We don’t know where to launch/share our product without paid marketing. We heard you out. Here is a blog dedicated to just that." />

        <meta itemProp="name" content="Aflo.io | Get Marketing and Sales Strategies Every Week" />
        <meta itemProp="description" content="Users to AFLO.io: We don’t know where to launch/share our product without paid marketing. We heard you out. Here is a blog dedicated to just that." />
        <meta itemProp="image" content="%PUBLIC_URL%/aflo-io-logo.jpg" />

        <meta property="og:url" content="https://aflo.io" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="Aflo.io | Get Marketing and Sales Strategies Every Week" />
        <meta property="og:description" content="Join aflo.io and get weekly marketing strategies and practices delivered straight into your inbox. A community for marketers and entrepreneurs." />
        <meta property="og:image" content="%PUBLIC_URL%/aflo-io-logo.jpg" />

        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="Aflo.io | Get Marketing and Sales Strategies Every Week" />
        <meta name="twitter:description" content="Join aflo.io and get weekly marketing strategies and practices delivered straight into your inbox. A community for marketers and entrepreneurs." />
        <meta name="twitter:image" content="%PUBLIC_URL%/aflo-io-logo.jpg" />
      </Helmet>
      <Route exact path={Routes.LANDING} component={Landing} />
      <Route exact path={Routes.ABOUT} component={About} />
      <Route exact path={Routes.CONTACT} component={Contact} />
      <Route exact path={Routes.BLOG_POST} component={BlogPost} />
      <Route exact path={Routes.FILTERED_POSTS} component={FilteredPosts} />
    </>
  );
}

const AuthenticatedRoutes = withAuthentication(AuthenticatedRoutesComponent);

function App(props) {
  return (
    <SnackbarProvider maxSnack={3}>
      <Router>
        <AuthenticatedRoutes {...props} />
        <AuthModal />
      </Router>
    </SnackbarProvider>
  );
}

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});

export default compose(
  connect(mapStateToProps, null),
)(App);
