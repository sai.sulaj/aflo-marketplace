// --------------- DEPENDENCIES -------------- //

import React from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Components.
import Navbar from '../../components/Navbar';
import Footer from '../../components/Footer';

// Assets.
import AboutImage from '../../assets/about-image.png';

// --------------- MAIN --------------- //

function About(props) {
  const classes = useStyles(props);

  return (
    <div className={classes.about}>
      <Navbar />
      <h1 className={classes.header}>
        No BS!
      </h1>
      <div className={classes.content}>
        <div className={classes.contentInner}>
          <div className={classes.columnLeft}>
            <img
              className={classes.aboutImage}
              height="300"
              width="300"
              alt=""
              src={AboutImage}
            />
          </div>
          <div className={classes.columnRight}>
            <p className={classes.textBody}>
              {/* eslint-disable-next-line */}
              The internet is flooded with marketing/sales strategies that are outdated, difficult to understand, and impossible to implement for the average entrepreneur. Entrepreneurs should not need to hire expensive resources to do marketing and sales, at least not in the beginning. The basics of marketing and sales are really just, basic!
              <br />
              <br />
              {/* eslint-disable-next-line */}
              That’s why we’ve created a better home for marketing and sales strategies—no other BS! We send you weekly strategies that you can easily implement and gain inspiration from to grow your business and acquire new customers.
              <br />
              <br />
              {/* eslint-disable-next-line */}
              Our strategies are specifically designed for startups and small and medium sized businesses so you can focus on what matters most: growing your hustle.
            </p>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default About;
