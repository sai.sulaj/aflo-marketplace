// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  about: {
    width: '100vw',
  },
  content: {
    width: '100vw',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '64px 2%',
    boxSizing: 'border-box',
    '@media only screen and (max-width: 600px)': {
      padding: '64px 15px',
    },
  },
  header: {
    width: '100%',
    paddingTop: 130,
    paddingBottom: 30,
    textAlign: 'center',
    margin: 0,
    fontFamily: "'Oxygen', sans-serif !important",
    fontSize: 40,
    color: Colors.DARK_GREY,
  },
  contentInner: {
    maxWidth: 980,
    width: '100%',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    boxSizing: 'border-box',
    '@media only screen and (max-width: 600px)': {
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  columnLeft: {
    boxSizing: 'border-box',
  },
  columnRight: {
    boxSizing: 'border-box',
    flex: 1,
    '@media only screen and (max-width: 600px)': {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
  aboutImage: {
    height: 300,
    width: 300,
    objectFit: 'cover',
    '@media only screen and (max-width: 600px)': {
      marginBottom: 15,
    },
  },
  textBody: {
    width: '100%',
    margin: 0,
    paddingLeft: 20,
    '@media only screen and (max-width: 600px)': {
      padding: 0,
      width: 300,
    },
  },
});
