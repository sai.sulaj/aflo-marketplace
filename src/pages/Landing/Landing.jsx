// --------------- DEPENDENCIES -------------- //

import React, {
  useRef,
} from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

import Hero from '../../components/Hero';
import TagRow from '../../components/TagRow';
import FeaturedPosts from '../../components/FeaturedPosts';
import RecentPosts from '../../components/RecentPosts';
import Footer from '../../components/Footer';

// --------------- MAIN --------------- //

function Landing(props) {
  const classes = useStyles(props);
  const scrollRef = useRef(null);

  function beginScroll() {
    if (scrollRef.current !== null) {
      scrollRef.current.scrollIntoView({ behavior: 'smooth' });
    }
  }

  return (
    <div className={classes.landing}>
      <Hero beginScroll={beginScroll} />
      <TagRow />
      <FeaturedPosts scrollRef={scrollRef} />
      <RecentPosts />
      <Footer backgroundColor="#E8E8E844" />
    </div>
  );
}

export default Landing;
