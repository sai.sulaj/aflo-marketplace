// ------------- DEPENDENCIES -------------- //

import { combineReducers } from 'redux';

// ------------- LOCAL DEPENDENCIES -------------- //

import sessionReducer from './session';
import authModalReducer from './authModal';

// ------------- MAIN -------------- //

const rootReducer = combineReducers({
  sessionState: sessionReducer,
  authModalState: authModalReducer,
});

export default rootReducer;
