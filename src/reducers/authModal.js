// ------------- LOCAL DEPENDENCIES -------------- //

import { ActionTypes } from '../constants';

// ------------- CONSTANTS & GLOBAL VARS -------------- //

const INITIAL_STATE = {
  open: false,
};

// ------------- MAIN -------------- //

const applySetOpen = (state, action) => ({
  ...state,
  open: action.open,
});

function authModalReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ActionTypes.AUTH_MODAL_OPEN_SET: {
      return applySetOpen(state, action);
    }
    default:
      return state;
  }
}

export default authModalReducer;
