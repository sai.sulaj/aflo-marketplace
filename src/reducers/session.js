// ------------- LOCAL DEPENDENCIES -------------- //

import { ActionTypes } from '../constants';

// ------------- CONSTANTS & GLOBAL VARS -------------- //

const INITIAL_STATE = {
  authUser: null,
};

// ------------- MAIN -------------- //

const applySetAuthUser = (state, action) => ({
  ...state,
  authUser: action.authUser,
});

function sessionReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ActionTypes.AUTH_USER_SET: {
      return applySetAuthUser(state, action);
    }
    default:
      return state;
  }
}

export default sessionReducer;
