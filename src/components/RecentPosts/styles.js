// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  recentPosts: {
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: `${Colors.LIGHT_GREY}44`,
  },
  contentContainer: {
    width: '100%',
    maxWidth: 940,
  },
  headerContainer: {
    width: '100%',
    textAlign: 'center',
  },
  header: {
    fontSize: 20,
    color: Colors.DARK_GREY,
    fontWeight: 400,
    display: 'inline-block',
    marginTop: 60,
    padding: '0 20px',
    paddingBottom: 15,
    borderBottom: `1px solid ${Colors.LIGHT_GREY}`,
  },
  postRow: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  recentPost: {
    height: 300,
    flex: '1 1 33%',
    minWidth: 300,
    padding: 10,
    boxSizing: 'border-box',
    borderRadius: 3,
    cursor: 'pointer',
  },
  recentPostImageContainer: {
    position: 'relative',
    height: 150,
  },
  recentPostImage: {
    height: '100%',
    width: '100%',
    objectFit: 'cover',
    borderRadius: '3px 3px 0 0',
  },
  recentPostContent: {
    backgroundColor: Colors.WHITE,
    boxSizing: 'border-box',
    border: `1px solid ${Colors.LIGHT_GREY}`,
    borderRadius: 3,
    transition: 'box-shadow 0.2s ease',
    height: '100%',
    width: '100%',
    '&:hover': {
      boxShadow: `${Colors.LIGHT_GREY} 0px 0px 0px 1px, rgba(0, 0, 0, .09) 0px 2px 23px 0px`,
    },
  },
  recentPostLink: {
    textDecoration: 'none',
  },
  tagsContainer: {
    position: 'absolute',
    top: 15,
    left: 10,
  },
  tag: {
    display: 'inline',
    color: Colors.WHITE,
    fontSize: 12,
    padding: '7px 9px 6px 9px',
    borderRadius: 2,
    marginRight: 10,
  },
  recentPostTextContainer: {
    width: '100%',
    padding: 13,
    boxSizing: 'border-box',
  },
  recentPostTitle: {
    color: Colors.DARK_GREY,
    fontSize: 16,
    margin: 0,
  },
  recentPostSubtitle: {
    color: `${Colors.BLACK}99`,
    fontSize: 13,
    fontWeight: 400,
    margin: 0,
    marginTop: 6,
  },
  recentPostBottomRow: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    borderTop: `1px solid ${Colors.LIGHT_GREY}`,
    boxSizing: 'border-box',
  },
  recentPostAuthor: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
  },
  recentPostAuthorImage: {
    height: 26,
    width: 26,
    borderRadius: 999,
    objectFit: 'cover',
  },
  recentPostAuthorName: {
    color: `${Colors.BLACK}99`,
    fontSize: 12,
    marginLeft: 10,
  },
  recentPostCreated: {
    color: `${Colors.BLACK}99`,
    fontSize: 12,
  },
  loadMoreButton: {
    marginTop: 20,
  },
});
