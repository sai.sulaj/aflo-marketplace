// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
  useEffect,
} from 'react';
import Moment from 'moment';
import Pose from 'react-pose';
import { Link } from 'react-router-dom';
import { withSnackbar } from 'notistack';

// --------------- LOCAL DEPENDENCIES -------------- //

// Components.
import WithLoader from '../WithLoader';
import Button from '../Button';

// Styles.
import { useStyles } from './styles';

// NetUtils.
import { DbNetUtils } from '../../networking';

// Constants.
import { Routes, Colors } from '../../constants';

// Utils.
import { safeAccess, URIUtils } from '../../utils';

// --------------- MAIN --------------- //

const PostPose = Pose.div({
  hoverable: true,
  init: {
    y: 0,
  },
  hover: {
    y: -5,
  },
});

export function RecentPost(props) {
  const classes = useStyles(props);
  const {
    tag1,
    tag2,
    title,
    subtitle,
    image,
    author,
    created_at,
  } = props;

  const safeTag1 = tag1 || {};
  const safeTag2 = tag2 || {};

  return (
    <PostPose className={classes.recentPost}>
      <div className={classes.recentPostContent}>
        <Link
          className={classes.recentPostLink}
          to={`${Routes.BLOG_POST}?title=${URIUtils.encodeBlogPostTitle(title)}`}
        >
          <div className={classes.recentPostImageContainer}>
            <img
              alt=""
              className={classes.recentPostImage}
              src={`${process.env.REACT_APP_URL_BASE}${safeAccess(['url'], image)}`}
            />
            <div className={classes.tagsContainer}>
              <div
                className={classes.tag}
                style={{
                  backgroundColor: safeTag1.color,
                }}
              >
                {safeTag1.label}
              </div>
              <div
                className={classes.tag}
                style={{
                  backgroundColor: safeTag2.color,
                }}
              >
                {safeTag2.label}
              </div>
            </div>
          </div>
          <div className={classes.recentPostTextContainer}>
            <h1 className={classes.recentPostTitle}>
              {title}
            </h1>
            <h3 className={classes.recentPostSubtitle}>
              {subtitle}
            </h3>
          </div>
          <div className={classes.recentPostBottomRow}>
            <div className={classes.recentPostAuthor}>
              <img
                className={classes.recentPostAuthorImage}
                src={`${process.env.REACT_APP_URL_BASE}${safeAccess(['image', 'url'], author)}`}
                alt=""
              />
              <span className={classes.recentPostAuthorName}>
                {safeAccess(['name'], author)}
              </span>
            </div>
            <span className={classes.recentPostCreated}>
              {created_at && Moment(created_at).format('D/MM/YYYY')}
            </span>
          </div>
        </Link>
      </div>
    </PostPose>
  );
}

function RecentPosts(props) {
  const classes = useStyles(props);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [numPosts, setNumPosts] = useState(-1);
  const { backgroundColor = `${Colors.LIGHT_GREY}44` } = props;

  async function fetchData() {
    setLoading(true);
    try {
      const newPosts = await DbNetUtils.doGetRecentBlogPosts(page, 6);
      setPosts([
        ...posts,
        ...newPosts,
      ]);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }
  async function countPosts() {
    try {
      const postCount = await DbNetUtils.doGetNumberOfPosts();
      setNumPosts(postCount);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    countPosts();
  }, []);

  useEffect(() => {
    fetchData();
  }, [page]);

  return (
    <div
      className={classes.recentPosts}
      style={{ backgroundColor }}
    >
      <div className={classes.contentContainer}>
        <div className={classes.headerContainer}>
          <h1 className={classes.header}>
            Recent Posts
          </h1>
        </div>
        <div className={classes.postRow}>
          <WithLoader loading={loading}>
            {posts.map((post) => (
              <RecentPost key={post.id} {...post} />
            ))}
          </WithLoader>
          {posts.length < numPosts && (
            <Button
              className={classes.loadMoreButton}
              loading={loading}
              size="medium"
              fullWidth
              onClick={() => setPage((prev) => prev + 6)}
            >
              Load more
            </Button>
          )}
        </div>
      </div>
    </div>
  );
}

export default withSnackbar(RecentPosts);
