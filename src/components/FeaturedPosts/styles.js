// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  featuredPosts: {
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: `${Colors.LIGHT_GREY}44`,
    borderTop: `1px solid ${Colors.LIGHT_GREY}`,
  },
  contentContainer: {
    width: '100%',
    maxWidth: 940,
  },
  headerContainer: {
    width: '100%',
    textAlign: 'center',
  },
  header: {
    fontSize: 20,
    color: Colors.DARK_GREY,
    fontWeight: 400,
    display: 'inline-block',
    marginTop: 60,
    padding: '0 20px',
    paddingBottom: 15,
    borderBottom: `1px solid ${Colors.LIGHT_GREY}`,
  },
  postRow: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  featuredPost: {
    position: 'relative',
    height: 350,
    flex: '1 1 300px',
    minWidth: 300,
    padding: 10,
    boxSizing: 'border-box',
    borderRadius: 3,
  },
  featuredPostImage: {
    position: 'absolute',
    top: 10,
    left: 10,
    height: 'calc(100% - 20px)',
    width: 'calc(100% - 20px)',
    objectFit: 'cover',
    borderRadius: 3,
  },
  featuredPostContent: {
    position: 'absolute',
    top: 10,
    left: 10,
    height: 'calc(100% - 20px)',
    width: 'calc(100% - 20px)',
    borderRadius: 3,
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'space-between',
    padding: 20,
    boxSizing: 'border-box',
    cursor: 'pointer',
    textDecoration: 'none',
    border: `1px solid ${Colors.LIGHT_GREY}`,
  },
  tagsContainer: {
  },
  tag: {
    display: 'inline',
    color: Colors.WHITE,
    fontSize: 12,
    padding: '7px 9px 6px 9px',
    borderRadius: 2,
    marginRight: 10,
  },
  featuredPostTextContainer: {
    boxSizing: 'border-box',
    width: 'calc(100% + 40px)',
    margin: '0 0 -20px -20px',
    backgroundColor: Colors.WHITE,
    padding: '10px 20px',
    borderRadius: '0 0 2px 2px',
  },
  featuredPostTitle: {
    color: Colors.DARK_GREY,
    fontSize: 27,
    margin: 0,
  },
  featuredPostSubtitle: {
    color: `${Colors.DARK_GREY}99`,
    fontSize: 14,
    fontWeight: 400,
    margin: 0,
    marginTop: 6,
  },
  featuredPostBottomRow: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
    borderTop: `1px solid ${Colors.LIGHT_GREY}`,
  },
  featuredPostAuthor: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
  },
  featuredPostAuthorImage: {
    height: 26,
    width: 26,
    borderRadius: 999,
    objectFit: 'cover',
  },
  featuredPostAuthorName: {
    color: `${Colors.DARK_GREY}99`,
    fontSize: 12,
    marginLeft: 10,
  },
  featuredPostCreated: {
    color: `${Colors.DARK_GREY}99`,
    fontSize: 12,
  },
});
