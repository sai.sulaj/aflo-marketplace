// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
  useEffect,
} from 'react';
import Moment from 'moment';
import Pose from 'react-pose';
import { Link } from 'react-router-dom';

// --------------- LOCAL DEPENDENCIES -------------- //

// Components.
import WithLoader from '../WithLoader';

// Styles.
import { useStyles } from './styles';

// Networking.
import { DbNetUtils } from '../../networking';

// Constants.
import { Routes } from '../../constants';

// Utils.
import { safeAccess, URIUtils } from '../../utils';

// --------------- MAIN --------------- //

const PostPose = Pose.div({
  hoverable: true,
  init: {
    y: 0,
  },
  hover: {
    y: -5,
  },
});

function FeaturedPost(props) {
  const classes = useStyles(props);
  const {
    tag1,
    tag2,
    title,
    subtitle,
    image,
    author,
    created_at,
  } = props;

  const safeTag1 = tag1 || {};
  const safeTag2 = tag2 || {};

  return (
    <PostPose className={classes.featuredPost}>
      <img
        alt=""
        className={classes.featuredPostImage}
        src={`${process.env.REACT_APP_URL_BASE}${safeAccess(['url'], image)}`}
      />
      <Link
        className={classes.featuredPostContent}
        to={`${Routes.BLOG_POST}?title=${URIUtils.encodeBlogPostTitle(title)}`}
      >
        <div className={classes.tagsContainer}>
          <div
            className={classes.tag}
            style={{
              backgroundColor: safeTag1.color,
            }}
          >
            {safeTag1.label}
          </div>
          <div
            className={classes.tag}
            style={{
              backgroundColor: safeTag2.color,
            }}
          >
            {safeTag2.label}
          </div>
        </div>
        <div className={classes.featuredPostTextContainer}>
          <h1 className={classes.featuredPostTitle}>
            {title}
          </h1>
          <h3 className={classes.featuredPostSubtitle}>
            {subtitle}
          </h3>
          <div className={classes.featuredPostBottomRow}>
            <div className={classes.featuredPostAuthor}>
              <img
                className={classes.featuredPostAuthorImage}
                src={`${process.env.REACT_APP_URL_BASE}${safeAccess(['image', 'url'], author)}`}
                alt=""
              />
              <span className={classes.featuredPostAuthorName}>
                {safeAccess(['name'], author)}
              </span>
            </div>
            <span className={classes.featuredPostCreated}>
              {created_at && Moment(created_at).format('MMMM Do, YYYY')}
            </span>
          </div>
        </div>
      </Link>
    </PostPose>
  );
}

function FeaturedPosts(props) {
  const classes = useStyles(props);
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const { scrollRef } = props;

  async function fetchData() {
    setLoading(true);
    try {
      const newPosts = await DbNetUtils.doGetFeaturedBlogPosts();
      setPosts(newPosts);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className={classes.featuredPosts}>
      <div className={classes.contentContainer}>
        <div className={classes.headerContainer}>
          <h1 className={classes.header}>
            Featured Posts
          </h1>
        </div>
        <div
          className={classes.postRow}
          ref={scrollRef}
        >
          <WithLoader loading={loading}>
            {posts.map((post) => (
              <FeaturedPost
                key={post.id}
                {...post}
              />
            ))}
          </WithLoader>
        </div>
      </div>
    </div>
  );
}

export default FeaturedPosts;
