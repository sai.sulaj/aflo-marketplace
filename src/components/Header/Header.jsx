// --------------- DEPENDENCIES -------------- //

import React from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// --------------- MAIN --------------- //

function Header(props) {
  const classes = useStyles(props);
  const { header } = props;

  return (
    <div className={classes.header}>
      <h1 className={classes.headerText}>
        {header}
      </h1>
    </div>
  );
}

export default Header;
