// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  header: (props) => ({
    height: 380,
    width: '100vw',
    backgroundImage: `url("${props.imageUrl}")`,
    backgroundSize: 'cover',
    backgroundPosition: '50% 50%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  headerText: {
    color: Colors.WHITE,
    fontSize: 50,
    fontWeight: 400,
    textAlign: 'center',
    fontFamily: "'Oxygen', sans-serif !important",
  },
});
