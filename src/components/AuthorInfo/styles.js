// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  authorInfo: {
    width: '100%',
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  image: {
    borderRadius: 999,
    height: 100,
    width: 100,
    objectFit: 'cover',
  },
  name: {
    fontSize: 17,
    margin: '10px 0',
    fontWeight: 600,
  },
  divider: {
    borderBottom: `2px solid ${Colors.MED_GREY}66`,
    marginBottom: 10,
    width: 50,
  },
  bio: {
    textAlign: 'center',
    fontSize: 13,
    maxWidth: 410,
    margin: 0,
    color: Colors.MED_GREY,
  },
});
