// --------------- DEPENDENCIES -------------- //

import React from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// --------------- MAIN --------------- //

function AuthorInfo(props) {
  const classes = useStyles(props);
  const { author } = props;
  const {
    name,
    image,
    bio,
  } = author || {};

  return (
    <div className={classes.authorInfo}>
      <img
        className={classes.image}
        src={`${process.env.REACT_APP_URL_BASE}${(image || {}).url}`}
        alt=""
      />
      <span className={classes.name}>
        {name}
      </span>
      <div className={classes.divider} />
      <p className={classes.bio}>
        {bio}
      </p>
    </div>
  );
}

export default AuthorInfo;
