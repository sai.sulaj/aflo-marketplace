// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  hero: {
    height: '65vh',
    paddingTop: 50,
    backgroundPosition: '0, 0, 50%, 50%',
    backgroundSize: 'auto, cover',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '@media only screen and (max-width: 600px)': {
      height: '80vh',
    },
  },
  contentContainer: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    width: '100%',
    maxWidth: 1100,
    justifyContent: 'space-between',
    boxSizing: 'border-box',
    '@media only screen and (max-width: 600px)': {
      flexFlow: 'column nowrap',
    },

    '& > *': {
      flex: 1,
    },
  },
  textContainer: {
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'flex-start',
    width: '100%',
    justifyContent: 'space-between',
    '@media only screen and (max-width: 600px)': {
      alignItems: 'center',
    },
  },
  heroImage: {
    height: '40vh',
    '@media only screen and (min-width: 600px)': {
      maxWidth: '50vw',
    },
    '@media only screen and (max-width: 600px)': {
      width: '80vw',
    },
  },
  heroTitle: {
    color: Colors.DARK_GREY,
    fontSize: '3.5em',
    fontWeight: 500,
    marginBottom: 14,
    fontFamily: "'Oxygen', sans-serif !important",
    '@media only screen and (max-width: 600px)': {
      textAlign: 'center',
    },
  },
  heroSubtitle: {
    marginTop: 0,
    fontSize: 18,
    color: Colors.DARK_GREY,
    fontWeight: 300,
    opacity: 0.63,
    '@media only screen and (max-width: 600px)': {
      textAlign: 'center',
    },
  },
  actionButton: {
    cursor: 'pointer',
    border: 'none',
    color: Colors.WHITE,
    borderRadius: 3,
    height: 44,
    width: 170,
    fontSize: 14,
    marginTop: 5,
    backgroundColor: Colors.BLUE,
    transition: 'background-color 50ms linear',
    '&:hover': {
      backgroundColor: `${Colors.BLUE}CC`,
    },
    '&:focus': {
      outline: 0,
    },
  },
});
