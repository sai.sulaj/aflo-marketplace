// --------------- DEPENDENCIES -------------- //

import React from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

import { useStyles } from './styles';

import Navbar from '../Navbar';

// --------------- MAIN --------------- //

function Hero(props) {
  const classes = useStyles(props);
  const { beginScroll } = props;

  return (
    <div className={classes.hero}>
      <Navbar />
      <div className={classes.contentContainer}>
        <div className={classes.textContainer}>
          <h1 className={classes.heroTitle}>
            Let&apos;s grow together.
          </h1>
          <h3 className={classes.heroSubtitle}>
            Building a community of marketers and entrepreneurs. Join us.
          </h3>
          <button
            className={classes.actionButton}
            type="button"
            onClick={() => beginScroll()}
          >
            View Latest Posts
          </button>
        </div>
        <div>
          <img
            className={classes.heroImage}
            src="https://uploads-ssl.webflow.com/5d773ffe5b3233122e861c07/5d7dc87da3db08b91535423f_undraw_status_update_jjgk.svg"
            alt=""
          />
        </div>
      </div>
    </div>
  );
}

export default Hero;
