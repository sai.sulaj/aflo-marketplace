// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
  useEffect,
} from 'react';
import { Link } from 'react-router-dom';
import Pose, { PoseGroup } from 'react-pose';

// --------------- LOCAL DEPENDENCIES -------------- //

// Network Utils.
import { DbNetUtils } from '../../networking';

// Styles.
import { useStyles } from './styles';

// Constants.
import { Routes } from '../../constants';

// --------------- MAIN --------------- //

const TagContainer = Pose.div({
  enter: {
    delay: 300,
    beforeChildren: true,
    staggerChildren: 60,
  },
  exit: {
    afterChildren: true,
  },
});
const TagInner = Pose.span({
  enter: {
    y: 0,
    transition: {
      y: {
        type: 'keyframes',
        values: [0, -10, 0],
        times: [0, 0.5, 1],
      },
    },
  },
  exit: {},
});

function TagRow(props) {
  const classes = useStyles(props);
  const [tags, setTags] = useState([]);

  async function fetchTags() {
    try {
      const newTags = await DbNetUtils.doGetTags();
      setTags(newTags);
    } catch (error) {
      console.error(error);
    }
  }

  useEffect(() => {
    fetchTags();
  }, []);

  return (
    <PoseGroup>
      <TagContainer
        key="container"
        className={classes.tagRow}
      >
        <div className={classes.tagRowInner}>
          {tags.map((tag) => (
            <Link
              key={tag.id}
              className={classes.tag}
              to={`${Routes.FILTERED_POSTS}?tag=${tag.id}`}
            >
              <TagInner
                key={tag.id}
                className={classes.tagInner}
              >
                {tag.label}
              </TagInner>
            </Link>
          ))}
        </div>
      </TagContainer>
    </PoseGroup>
  );
}

export default TagRow;
