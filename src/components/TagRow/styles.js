// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  tagRow: {
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tagRowInner: {
    width: '100%',
    height: '100%',
    maxWidth: 1100,
    display: 'flex',
    flexFlow: 'row wrap',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  tag: {
    height: 60,
    fontSize: 14,
    width: 100,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textDecoration: 'none',
  },
  tagInner: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: Colors.DARK_GREY,
    textAlign: 'center',
    cursor: 'pointer',
    height: '70%',
    width: '100%',
    borderRadius: 3,
    transition: 'background-color 150ms',
    fontWeight: 700,
    '&:hover': {
      backgroundColor: Colors.LIGHT_GREY,
      color: Colors.BLACK,
    },
  },
});
