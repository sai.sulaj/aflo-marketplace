// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  authModal: {
    position: 'fixed',
    top: 0,
    left: 0,
    height: '100vh',
    width: '100vw',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: `${Colors.BLACK}66`,
    boxSizing: 'border-box',
    padding: 20,
    zIndex: 100,
  },
  contentWrapper: {
    padding: '20px 40px',
    borderRadius: 3,
    backgroundColor: Colors.WHITE,
    boxShadow: `${Colors.LIGHT_GREY} 0px 0px 0px 1px, ${Colors.BLUE}66 0px 2px 23px 0px`,
    maxWidth: 400,
    width: '100%',
  },
  content: {
    borderRadius: 3,
    width: '100%',
    '& > *:not(:last-child)': {
      marginBottom: 15,
    },
  },
  header: {
    margin: 0,
    paddingBottom: 20,
    fontSize: 30,
    width: '100%',
    textAlign: 'center',
    fontFamily: "'Oxygen', sans-serif !important",
    fontWeight: 700,
    letterSpacing: 2.5,
  },
  submitButton: {
    marginTop: 30,
  },
  error: {
    color: Colors.RED,
    fontSize: 14,
  },
});
