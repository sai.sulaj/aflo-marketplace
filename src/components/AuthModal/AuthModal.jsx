// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
  useEffect,
} from 'react';
import {
  TiMail,
  TiLockClosedOutline,
  TiUserOutline,
} from 'react-icons/ti';
import Pose from 'react-pose';
import { connect } from 'react-redux';
import { compose } from 'recompose';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Components.
import InputField from '../InputField';
import Button from '../Button';

// Hooks.
import { useOutsideClickAlert } from '../../hooks';

// Constants.
import {
  Colors,
  ActionTypes,
} from '../../constants';


// Networking.
import { AuthUtils } from '../../firebase';

// --------------- CONSTANTS --------------- //

const VIEWS = {
  SIGN_UP: 0,
  SIGN_IN: 1,
};

// --------------- MAIN --------------- //

const ModalContainer = Pose.div({
  open: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    applyAtStart: {
      display: 'flex',
    },
  },
  closed: {
    backgroundColor: 'rgba(0, 0, 0, 0)',
    applyAtEnd: {
      display: 'none',
    },
  },
});
const ContentContainer = Pose.div({
  open: {
    y: '0vh',
  },
  closed: {
    y: '80vh',
  },
});

function SignInView(props) {
  const classes = useStyles(props);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const {
    toSignUp,
    doSetOpen,
  } = props;

  async function handleSubmit(event) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    setError(null);
    setLoading(true);
    try {
      await AuthUtils.doSignInWithEmailAndPassword(email, password);
      doSetOpen(false);
    } catch (newError) {
      console.error(newError);
      setError(newError.message);
    } finally {
      setLoading(false);
    }
  }

  return (
    <form
      className={classes.content}
      onSubmit={handleSubmit}
    >
      <h1 className={classes.header}>
        LOG IN
      </h1>
      <InputField
        fullWidth
        icon={TiMail}
        iconColor={Colors.MED_GREY}
        placeholder="Email"
        type="email"
        value={email}
        onChange={setEmail}
      />
      <InputField
        fullWidth
        icon={TiLockClosedOutline}
        iconColor={Colors.MED_GREY}
        placeholder="Password"
        type="password"
        value={password}
        onChange={setPassword}
      />
      {error && (
        <span className={classes.error}>
          {error}
        </span>
      )}
      <Button
        className={classes.submitButton}
        size="medium"
        fullWidth
        loading={loading}
        type="submit"
      >
        Sign In
      </Button>
      <Button
        secondary
        size="medium"
        fullWidth
        loading={loading}
        onClick={toSignUp}
      >
        Sign Up
      </Button>
    </form>
  );
}

function SignUpView(props) {
  const classes = useStyles(props);
  const [email, setEmail] = useState('');
  const [displayName, setDisplayName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const {
    toSignIn,
    doSetOpen,
  } = props;

  async function handleSubmit(event) {
    if (event && event.preventDefault) {
      event.preventDefault();
    }

    if (displayName === '') {
      setError('Please enter a valid name.');
      return;
    }

    if (password !== confirmPassword) {
      setError('Passwords do not match.');
      return;
    }

    setError(null);
    setLoading(true);
    try {
      await AuthUtils.doCreateUserWithEmailAndPassword(email, password, displayName);
      doSetOpen(false);
    } catch (newError) {
      console.error(newError);
      setError(newError.message);
    } finally {
      setLoading(false);
    }
  }

  return (
    <form
      className={classes.content}
      onSubmit={handleSubmit}
    >
      <h1 className={classes.header}>
        SIGN UP
      </h1>
      <InputField
        fullWidth
        icon={TiUserOutline}
        iconColor={Colors.MED_GREY}
        placeholder="Name"
        type="name"
        value={displayName}
        onChange={setDisplayName}
      />
      <InputField
        fullWidth
        icon={TiMail}
        iconColor={Colors.MED_GREY}
        placeholder="Email"
        type="email"
        value={email}
        onChange={setEmail}
      />
      <InputField
        fullWidth
        icon={TiLockClosedOutline}
        iconColor={Colors.MED_GREY}
        placeholder="Password"
        type="password"
        value={password}
        onChange={setPassword}
      />
      <InputField
        fullWidth
        icon={TiLockClosedOutline}
        iconColor={Colors.MED_GREY}
        placeholder="Confirm Password"
        type="password"
        value={confirmPassword}
        onChange={setConfirmPassword}
      />
      {error && (
        <span className={classes.error}>
          {error}
        </span>
      )}
      <Button
        className={classes.submitButton}
        size="medium"
        fullWidth
        loading={loading}
        type="submit"
      >
        Sign Up
      </Button>
      <Button
        secondary
        size="medium"
        fullWidth
        loading={loading}
        onClick={toSignIn}
      >
        Log In
      </Button>
    </form>
  );
}

function AuthModal(props) {
  const classes = useStyles(props);
  const [activeView, setActiveView] = useState(VIEWS.SIGN_UP);
  const {
    open,
    doSetOpen,
  } = props;

  const outsideClickRef = useOutsideClickAlert(() => doSetOpen(false));

  useEffect(() => {
    if (!open) {
      setActiveView(VIEWS.SIGN_UP);
    }
  }, [open]);

  return (
    <ModalContainer
      className={classes.authModal}
      pose={open ? 'open' : 'closed'}
    >
      <ContentContainer
        className={classes.contentWrapper}
        open={open}
        ref={outsideClickRef}
      >
        {activeView === VIEWS.SIGN_IN && (
          <SignInView
            toSignUp={() => setActiveView(VIEWS.SIGN_UP)}
            {...props}
          />
        )}
        {activeView === VIEWS.SIGN_UP && (
          <SignUpView
            toSignIn={() => setActiveView(VIEWS.SIGN_IN)}
            {...props}
          />
        )}
      </ContentContainer>
    </ModalContainer>
  );
}

const mapStateToProps = (state) => ({
  open: state.authModalState.open,
});
const mapDispatchToProps = (dispatch) => ({
  doSetOpen: (open) => dispatch({
    type: ActionTypes.AUTH_MODAL_OPEN_SET,
    open,
  }),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(AuthModal);
