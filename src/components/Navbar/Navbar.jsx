// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
} from 'react';
import { NavLink } from 'react-router-dom';
import Pose from 'react-pose';
import {
  TiThMenu,
  TiTimes,
} from 'react-icons/ti';
import { GoMail } from 'react-icons/go';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import classnames from 'classnames';

// --------------- LOCAL DEPENDENCIES -------------- //

// Networking.
import { AuthUtils } from '../../firebase';

// Components.
import Button from '../Button';

import { useStyles } from './styles';
import {
  Routes,
  ActionTypes,
} from '../../constants';
import logo from '../../assets/logo.png';

// --------------- MAIN --------------- //

const navlinks = [
  {
    label: 'about',
    to: Routes.ABOUT,
  },
  {
    label: 'contact',
    to: Routes.CONTACT,
  },
];

const AnimatedNavLink = Pose.div({
  hoverable: true,
  init: {
    opacity: 0.7,
    y: 0,
  },
  active: {
    opacity: 1,
    y: 0,
  },
  hover: {
    opacity: 1,
    y: -2,
  },
});

function AuthenticatedUserItem(props) {
  const [open, setOpen] = useState(false);
  const classes = useStyles(props);
  const { authUser } = props;

  return (
    <span
      className={classes.greeting}
      onMouseEnter={() => setOpen(true)}
      onMouseLeave={() => setOpen(false)}
    >
      hello
      <span className={classes.userName}>
        {authUser.displayName}
      </span>
      <div
        className={classnames(
          classes.greetingDropdown,
          open && classes.greetingDropdownOpen,
        )}
      >
        <div className={classes.greetingDropdownContent}>
          <Button
            className={classes.signOutButton}
            size="medium"
            onClick={AuthUtils.doSignOut}
          >
            Sign Out
          </Button>
        </div>
      </div>
    </span>
  );
}

function DesktopNavlinks(props) {
  const classes = useStyles(props);
  const [activeLink, setActiveLink] = useState(false);
  const {
    isAuthenticated,
    doSetAuthModalOpen,
    authUser,
  } = props;

  const handleActiveLinkSet = (match, location) => {
    if (match) {
      setActiveLink(location.pathname);
    }
  };

  return (
    <div className={classes.navlinksContainerDesktop}>
      {navlinks.map((navlink) => (
        <NavLink
          isActive={handleActiveLinkSet}
          className={classes.navlink}
          exact
          key={navlink.to}
          to={navlink.to}
        >
          <AnimatedNavLink
            pose={activeLink === navlink.to ? 'active' : undefined}
          >
            <span>{navlink.label}</span>
          </AnimatedNavLink>
        </NavLink>
      ))}
      {isAuthenticated ? (
        <AuthenticatedUserItem authUser={authUser} />
      ) : (
        <Button
          className={classes.signUpButton}
          size="medium"
          onClick={() => doSetAuthModalOpen(true)}
        >
          Sign Up
        </Button>
      )}
    </div>
  );
}
const AnimatedMobileNavLinksList = Pose.div({
  closed: {
    x: '100%',
    transition: {
      type: 'tween',
      duration: 350,
    },
  },
  open: {
    x: '0%',
    transition: {
      type: 'tween',
      duration: 350,
    },
  },
});
function MobileNavLinksList(props) {
  const classes = useStyles(props);
  const {
    open,
    setOpen,
    isAuthenticated,
    doSetAuthModalOpen,
    authUser,
  } = props;
  const [activeLink, setActiveLink] = useState(false);

  const handleActiveLinkSet = (match, location) => {
    if (match) {
      setActiveLink(location.pathname);
    }
  };

  return (
    <AnimatedMobileNavLinksList
      className={classes.mobileNavlinksSidebar}
      pose={open ? 'open' : 'closed'}
    >
      <button
        className={classes.mobileSidebarCloseButton}
        onClick={() => setOpen(false)}
        type="button"
      >
        <TiTimes />
      </button>
      {navlinks.map((navlink) => (
        <NavLink
          isActive={handleActiveLinkSet}
          className={classes.navlinkMobile}
          exact
          key={navlink.to}
          to={navlink.to}
        >
          <AnimatedNavLink
            className={classes.navLinkTextMobile}
            pose={activeLink === navlink.to ? 'active' : undefined}
          >
            <span>{navlink.label}</span>
          </AnimatedNavLink>
        </NavLink>
      ))}
      {isAuthenticated ? (
        <span className={classes.greetingMobile}>
          Hello
          <span className={classes.userName}>
            {authUser.displayName}
          </span>
        </span>
      ) : (
        <Button
          fullWidth
          className={classes.signUpButtonMobile}
          size="medium"
          onClick={() => doSetAuthModalOpen(true)}
        >
          Sign Up
        </Button>
      )}
    </AnimatedMobileNavLinksList>
  );
}
function MobileNavLinks(props) {
  const classes = useStyles(props);
  const [open, setOpen] = useState(false);

  return (
    <div className={classes.navlinksContainerMobile}>
      <button
        onClick={() => setOpen(!open)}
        className={classes.toggleMenuButton}
        type="button"
      >
        <span>MENU</span>
        <TiThMenu className={classes.menuIconMobile} />
      </button>
      <MobileNavLinksList
        open={open}
        setOpen={setOpen}
        {...props}
      />
    </div>
  );
}

function Navbar(props) {
  const classes = useStyles(props);
  const {
    authUser,
    doSetAuthModalOpen,
  } = props;
  const [input, setInput] = useState('');

  const isAuthenticated = authUser && authUser.uid;

  return (
    <div className={classes.navbar}>
      <div className={classes.navbarInner}>
        <div className={classes.leftContainer}>
          <NavLink
            className={classes.logo}
            to={Routes.LANDING}
          >
            <img
              className={classes.logo}
              alt="logo"
              src={logo}
            />
            <span>.io</span>
          </NavLink>
          <form
            className={classes.emailInputContainer}
            action="https://aflo.us18.list-manage.com/subscribe/post"
          >
            <div className={classes.emailInputWrapper}>
              <input type="hidden" name="u" value="ca3c5e76ff2950e68b0f0dc7a" />
              <input type="hidden" name="id" value="914d4c5499" />
              <GoMail className={classes.emailIcon} />
              <input
                className={classes.emailInput}
                placeholder="Get new strategies every week"
                onChange={(event) => setInput(event.target.value)}
                value={input}
                name="MERGE0"
                id="MERGE0"
                type="email"
              />
            </div>
            <Button
              size="medium"
              type="submit"
            >
              Stay Updated
            </Button>
          </form>
        </div>
        <DesktopNavlinks
          authUser={authUser}
          isAuthenticated={isAuthenticated}
          doSetAuthModalOpen={doSetAuthModalOpen}
        />
        <MobileNavLinks
          authUser={authUser}
          isAuthenticated={isAuthenticated}
          doSetAuthModalOpen={doSetAuthModalOpen}
        />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});
const mapDispatchToProps = (dispatch) => ({
  doSetAuthModalOpen: (open) => dispatch({
    type: ActionTypes.AUTH_MODAL_OPEN_SET,
    open,
  }),
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(Navbar);
