// ---------------- DEPENDENCIES --------------- //

import chroma from 'chroma-js';
import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  navbar: {
    width: '100vw',
    height: 74,
    boxSizing: 'border-box',
    padding: '0 32px',
    display: 'flex',
    justifyContent: 'center',
    position: 'fixed',
    backgroundColor: Colors.WHITE,
    top: 0,
    left: 0,
    zIndex: 99,
  },
  navbarInner: {
    width: '100%',
    maxWidth: 940,
    height: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  logo: {
    height: '80%',
    display: 'flex',
    flexFlow: 'row nowwrap',
    alignItems: 'center',
    textDecoration: 'none',
    '& > span': {
      color: Colors.BLUE,
      fontFamily: '"Pacifico", cursive !important',
      transform: 'translateX(-2px)',
    },
  },
  navlinksContainerDesktop: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'flex-end',
    alignItems: 'center',
    '@media only screen and (max-width: 600px)': {
      display: 'none',
    },
  },
  navlinksContainerMobile: {
    display: 'flex',
    flexFlow: 'row nowrap',
    justifyContent: 'flex-end',
    alignItems: 'center',
    '@media only screen and (min-width: 600px)': {
      display: 'none',
    },
  },
  toggleMenuButton: {
    cursor: 'pointer',
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    color: Colors.WHITE,
    '&:focus': {
      outline: 0,
    },
    '& > span': {
      marginRight: 10,
    },
  },
  menuIconMobile: {
    color: Colors.BLUE,
    height: 20,
    width: 20,
  },
  navlink: {
    textDecoration: 'none',
    color: Colors.DARK_GREY,
    paddingLeft: 40,
    fontSize: 16,
    fontWeight: 600,
    letterSpacing: 0.6,
  },
  mobileNavlinksSidebar: {
    position: 'fixed',
    zIndex: 99,
    right: 0,
    top: 0,
    width: '100vw',
    height: '100vh',
    backgroundColor: `${Colors.DARK_BLUE}EE`,
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: '0 20px',
    boxSizing: 'border-box',
  },
  mobileSidebarCloseButton: {
    width: '100%',
    display: 'flex',
    flexFlow: 'column nowrap',
    alignItems: 'center',
    justifyContent: 'center',
    height: 60,
    padding: '10px 0',
    '& > svg': {
      color: Colors.WHITE,
      height: 20,
      width: 20,
    },
  },
  navlinkMobile: {
    textDecoration: 'none',
    color: Colors.WHITE,
    fontSize: 17,
    fontWeight: 300,
    letterSpacing: 0.6,
    width: '100%',
    textAlign: 'center',
  },
  navLinkTextMobile: {
    padding: '15px 0',
  },
  signUpButton: {
    marginLeft: 40,
  },
  signUpButtonMobile: {
    marginTop: 20,
    alignSelf: 'center',
  },
  greeting: {
    marginLeft: 40,
    color: Colors.DARK_GREY,
    fontWeight: 600,
    position: 'relative',
  },
  greetingContent: {
  },
  greetingMobile: {
    color: Colors.LIGHT_GREY,
    width: '100%',
    textAlign: 'center',
  },
  greetingDropdown: {
    position: 'absolute',
    top: 25,
    right: 0,
    maxHeight: 0,
    transition: 'max-height 200ms linear',
    overflow: 'hidden',
    backgroundColor: Colors.WHITE,
    borderRadius: 4,
  },
  greetingDropdownOpen: {
    maxHeight: '30vh',
  },
  greetingDropdownContent: {
  },
  userName: {
    color: chroma(Colors.BLUE).saturate().hex(),
    textShadow: `1px 1px 2px ${Colors.WHITE}33`,
    marginLeft: 10,
  },
  emailInputContainer: {
    height: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    paddingLeft: 20,
    '@media only screen and (max-width: 600px)': {
      display: 'none',
    },
  },
  emailInputWrapper: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    height: 41,
    width: 250,
    backgroundColor: Colors.LIGHT_BLUE,
    borderRadius: 3,
    paddingLeft: 12,
    boxSizing: 'border-box',
    marginRight: 5,
  },
  emailInput: {
    backgroundColor: Colors.LIGHT_BLUE,
    color: Colors.MED_GREY,
    width: '100%',
    padding: '0 8px',
    border: 'none',
    fontSize: 14,
    flex: 1,
    '&:focus': {
      outline: 0,
    },
  },
  leftContainer: {
    height: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
  },
  emailIcon: {
    color: Colors.MED_GREY,
    fontSize: 20,
  },
});
