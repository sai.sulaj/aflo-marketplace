// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
  useEffect,
  useRef,
} from 'react';
import Pose, {
  PoseGroup,
} from 'react-pose';
import Moment from 'moment';
import Loader from 'react-loader-spinner';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withSnackbar } from 'notistack';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Constants.
import { Colors, ActionTypes } from '../../constants';

// Networking.
import { DbNetUtils } from '../../networking';

// Hooks.
import { useOutsideClickAlert } from '../../hooks';

// Components.
import Button from '../Button';

// --------------- CONSTANTS --------------- //

const ROWS = 2;

// --------------- MAIN --------------- //

const CommentPose = Pose.div({
  hoverable: true,
  init: {
    y: 0,
  },
  hover: {
    y: -5,
  },
});
const LoaderPose = Pose.div({
  hidden: {
    height: 0,
    opacity: 0,
    transition: {
      height: { delay: 300 },
    },
    applyAtEnd: { display: 'none' },
  },
  loading: {
    height: 100,
    opacity: 1,
    transition: {
      opacity: {
        delay: 500,
        duration: 500,
      },
    },
    applyAtStart: { display: 'flex' },
  },
});

function Comment(props) {
  const classes = useStyles(props);
  const { comment = {} } = props;
  const {
    message,
    displayName,
    created_at,
  } = comment;

  return (
    <CommentPose className={classes.comment}>
      <div className={classes.commentHeaderRow}>
        <div className={classes.commentMetaColumn}>
          <h4 className={classes.commentAuthorName}>
            {displayName}
          </h4>
          <p className={classes.commentDate}>
            {created_at && Moment(created_at).format('MMM DD')}
          </p>
        </div>
      </div>
      <p className={classes.commentText}>
        {message}
      </p>
    </CommentPose>
  );
}

function Loading(props) {
  const classes = useStyles(props);
  const { loading } = props;

  return (
    <LoaderPose
      className={classes.loading}
      pose={loading ? 'loading' : 'hidden'}
    >
      <Loader
        type="Grid"
        color={Colors.BLUE}
        height={50}
        width={50}
      />
    </LoaderPose>
  );
}

const CommentInputContainer = Pose.div({
  open: {
    height: 300,
    transition: {
      type: 'tween',
      ease: 'anticipate',
    },
  },
  closed: {
    height: 60,
    transition: {
      type: 'tween',
      ease: 'anticipate',
    },
  },
});

const CommentInputTrigger = Pose.span({
  exit: {
    opacity: 0,
    y: -30,
    transition: {
      type: 'tween',
      duration: 150,
    },
  },
  enter: {
    opacity: 1,
    y: 0,
    transition: {
      type: 'tween',
      duration: 150,
    },
  },
});

const CommentUserName = Pose.span({
  exit: {
    opacity: 0,
    y: 30,
    transition: {
      type: 'tween',
      duration: 150,
    },
  },
  enter: {
    opacity: 1,
    y: 0,
    transition: {
      type: 'tween',
      duration: 150,
    },
  },
});

const CommentSubmit = Pose.div({
  open: {
    opacity: 1,
    applyAtStart: { display: 'inline-block' },
    transition: {
      type: 'tween',
      delay: 150,
    },
  },
  closed: {
    opacity: 0,
    applyAtEnd: { display: 'none' },
    transition: {
      type: 'tween',
    },
  },
});

function CommentInput(props) {
  const classes = useStyles(props);
  const [message, setMessage] = useState('');
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [submitCompleted, setSubmitCompleted] = useState(false);
  const {
    submit,
    displayName,
    onError,
  } = props;

  const outsideClickRef = useOutsideClickAlert(() => setOpen(false));
  const inputFieldRef = useRef(null);

  function openInput() {
    setOpen(true);
    if (inputFieldRef.current) {
      inputFieldRef.current.focus();
    }
  }

  async function submitComplete() {
    setSubmitCompleted(true);
    await new Promise((resolve) => setTimeout(resolve, 2000));
    setSubmitCompleted(false);
    setOpen(false);
    setMessage('');
  }
  async function handleSubmit() {
    setLoading(true);
    try {
      await submit(message);
      submitComplete();
    } catch (error) {
      console.error(error);
      onError('There was a problem, please try again later.');
      setOpen(false);
      setMessage('');
    } finally {
      setLoading(false);
    }
  }

  let buttonText = 'Submit';
  if (submitCompleted) {
    buttonText = 'Submitted!';
  }

  return (
    <CommentInputContainer
      className={classes.commentInput}
      pose={open ? 'open' : 'closed'}
      onClick={openInput}
      ref={outsideClickRef}
    >
      <div className={classes.commentInputTrigger}>
        <PoseGroup>
          {open ? (
            <CommentUserName
              key="comment-user-name"
              className={classes.commentInputTriggerUserName}
            >
              <span>{displayName}</span>
            </CommentUserName>
          ) : (
            <CommentInputTrigger
              key="trigger-label"
              className={classes.commentInputTriggerLabel}
            >
              Write a response...
            </CommentInputTrigger>
          )}
        </PoseGroup>
      </div>
      <textarea
        ref={inputFieldRef}
        className={classnames(
          classes.commentInputField,
          open && classes.commentInputFieldOpen,
        )}
        value={message}
        onChange={(event) => setMessage(event.target.value)}
      />
      <CommentSubmit
        pose={open ? 'open' : 'closed'}
      >
        <Button
          fullWidth
          onClick={() => handleSubmit()}
          notClickable={loading}
          loading={loading}
          size="medium"
        >
          {buttonText}
        </Button>
      </CommentSubmit>
    </CommentInputContainer>
  );
}

function Comments(props) {
  const classes = useStyles(props);
  const {
    blogPostId,
    authUser,
    enqueueSnackbar,
    onAuthModelOpenSet,
  } = props;
  const [comments, setComments] = useState([]);
  const [from, setFrom] = useState(0);
  const [loading, setLoading] = useState(false);

  function handleError(error = {}) {
    enqueueSnackbar(error.message || error, { variant: 'error' });
  }

  async function fetchComments(previous = []) {
    setLoading(true);
    try {
      const newComments = await DbNetUtils.doGetComments(blogPostId, from, ROWS);
      setComments([
        ...previous,
        ...newComments,
      ]);
    } catch (error) {
      console.error(error);
      handleError('There was a problem, please try again later.');
    } finally {
      setLoading(false);
    }
  }

  async function submitComment(message) {
    await DbNetUtils.doSubmitComment(blogPostId, authUser.displayName, message);
    fetchComments();
  }

  useEffect(() => {
    fetchComments(comments);
  }, [from, blogPostId]);

  return (
    <div className={classes.comments}>
      <h1 className={classes.header}>
        Comments
      </h1>
      <div className={classes.commentsContainer}>
        {authUser ? (
          <CommentInput
            onError={handleError}
            displayName={authUser && authUser.displayName}
            submit={submitComment}
          />
        ) : (
          <Button
            fullWidth
            className={classes.signUpButton}
            size="medium"
            onClick={() => onAuthModelOpenSet(true)}
          >
            Sign Up to Add a Comment
          </Button>
        )}
        {comments.map((comment) => (
          <Comment key={comment.id} comment={comment} />
        ))}
        {!loading && comments.length === 0 && (
          <span className={classes.noCommentsText}>
            Be the first to leave a comment
          </span>
        )}
        <Loading loading={loading} />
        <Button
          disabled={comments.length < from + ROWS || loading}
          onClick={() => setFrom(from + ROWS)}
          loading={loading}
          fullWidth
          size="large"
        >
          Load More
        </Button>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  authUser: state.sessionState.authUser,
});
const mapDispatchToProps = (dispatch) => ({
  onAuthModelOpenSet: (open) => dispatch({
    type: ActionTypes.AUTH_MODAL_OPEN_SET,
    open,
  }),
});

export default compose(
  withSnackbar,
  connect(mapStateToProps, mapDispatchToProps),
)(Comments);
