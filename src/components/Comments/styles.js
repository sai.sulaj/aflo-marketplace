// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  comments: {
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 30,
  },
  commentsContainer: {
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'flex-start',
    alignItems: 'center',
    maxWidth: 700,
    width: '100%',
    padding: '0 20px',
    boxSizing: 'border-box',
  },
  comment: {
    width: '100%',
    padding: 20,
    marginBottom: 20,
    borderRadius: 3,
    backgroundColor: Colors.WHITE,
    boxSizing: 'border-box',
    border: `1px solid ${Colors.LIGHT_GREY}`,
    transition: 'box-shadow 0.2s ease',
    '&:hover': {
      boxShadow: `${Colors.LIGHT_GREY} 0px 0px 0px 1px, rgba(0, 0, 0, .09) 0px 2px 23px 0px`,
    },
  },
  header: {
    margin: 0,
    marginBottom: 20,
    color: Colors.DARK_GREY,
  },
  loading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentHeaderRow: {
    width: '100%',
    display: 'flex',
    flexFlow: 'row nowrap',
  },
  commentMetaColumn: {
    width: '100%',
    paddingLeft: 10,
  },
  authorImage: {
    height: 40,
    width: 40,
    objectFit: 'cover',
    borderRadius: 999,
  },
  commentAuthorName: {
    margin: 0,
    fontWeight: 400,
    color: Colors.BLUE,
  },
  commentDate: {
    color: Colors.MED_GREY,
    margin: 0,
    marginTop: 3,
    fontSize: 13,
  },
  commentText: {
    color: Colors.DARK_GREY,
  },
  commentInput: {
    display: 'flex',
    flexFlow: 'column nowrap',
    boxSizing: 'border-box',
    width: '100%',
    backgroundColor: Colors.WHITE,
    border: `1px solid ${Colors.LIGHT_GREY}`,
    transition: 'box-shadow 0.2s ease',
    borderRadius: 3,
    padding: 20,
    marginBottom: 20,
    overflow: 'hidden',
    '&:hover': {
      boxShadow: `${Colors.LIGHT_GREY} 0px 0px 0px 1px, rgba(0, 0, 0, .09) 0px 2px 23px 0px`,
    },
  },
  commentInputTrigger: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    boxSizing: 'border-box',
  },
  commentInputTriggerLabel: {
    width: '100%',
    border: 'none',
    fontSize: 16,
    padding: '0 20px',
    color: Colors.MED_GREY,
  },
  commentInputTriggerUserName: {
    width: '100%',
    border: 'none',
    fontSize: 16,
    padding: '0 20px',
    color: Colors.BLUE,
  },
  commentInputField: {
    width: '100%',
    flex: 1,
    border: 'none',
    fontSize: 16,
    padding: 0,
    transition: 'padding 100ms linear 50ms',
    boxSizing: 'border-box',
    resize: 'none',
    '&:focus': {
      outline: 0,
    },
  },
  noCommentsText: {
    paddingTop: 20,
    paddingBottom: 40,
    color: Colors.MED_GREY,
    fontSize: 18,
  },
  commentInputFieldOpen: {
    padding: '20px 3px',
  },
  signUpButton: {
    marginBottom: 20,
  },
});
