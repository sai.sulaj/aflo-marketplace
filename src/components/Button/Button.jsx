// --------------- DEPENDENCIES -------------- //

import React from 'react';
import classnames from 'classnames';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// Constants.
import { Colors } from '../../constants';

// --------------- MAIN --------------- //

function Button(props) {
  const classes = useStyles(props);
  const {
    className,
    children,
    size,
    loading,
    fullWidth,
    disabled,
    notClickable,
    secondary,
    ...other
  } = props;
  const sizeClass = classes[size];

  return (
    <button
      className={classnames(
        className,
        classes.button,
        sizeClass,
        loading && classes.loading,
        fullWidth && classes.fullWidth,
        disabled && classes.disabled,
        secondary ? classes.secondary : classes.primary,
      )}
      disabled={notClickable || disabled}
      type="button"
      {...other}
    >
      {loading ? (
        <Loader
          className={classes.loader}
          type="Bars"
          color={Colors.WHITE}
          height={25}
        />
      ) : (children)}
    </button>
  );
}

export default Button;
