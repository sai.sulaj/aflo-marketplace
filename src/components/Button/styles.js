// ---------------- DEPENDENCIES --------------- //

import chroma from 'chroma-js';
import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  button: {
    border: 'none',
    borderRadius: 3,
    boxSizing: 'border-box',
    cursor: 'pointer',
    transition: 'background-color 50ms linear',
    whiteSpace: 'nowrap',
    '&:disabled': {
      cursor: 'unset',
    },
  },
  primary: {
    color: Colors.WHITE,
    backgroundColor: Colors.BLUE,
    '&:not([disabled])': {
      '&:focus': {
        outline: 0,
      },
      '&:hover': {
        backgroundColor: chroma(Colors.BLUE).brighten(0.3).hex(),
      },
      '&:active': {
        backgroundColor: chroma(Colors.BLUE).saturate(0.3).hex(),
      },
    },
  },
  secondary: {
    color: Colors.BLUE,
    background: 'none',
    '&:not([disabled])': {
      '&:focus': {
        outline: 0,
      },
      '&:hover': {
        backgroundColor: chroma(Colors.BLUE).alpha(0.1).hex(),
      },
      '&:active': {
        backgroundColor: chroma(Colors.BLUE).alpha(0.3).hex(),
      },
    },
  },
  disabled: {
    backgroundColor: Colors.MED_GREY,
  },
  medium: {
    fontSize: 14,
    padding: '12px 31px',
    height: 41,
  },
  large: {
    fontSize: 18,
    padding: '14px 35px',
    height: 52,
  },
  loading: {
    padding: 5,
  },
  fullWidth: {
    width: '100%',
  },
  loader: {
    transform: 'translateY(2px)',
  },
});
