// --------------- DEPENDENCIES -------------- //

import React from 'react';
import Loader from 'react-loader-spinner';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';

// --------------- LOCAL DEPENDENCIES -------------- //

import { Colors } from '../../constants';

// --------------- MAIN --------------- //

function WithLoader(props) {
  const {
    loading,
    children,
  } = props;

  return (
    <>
      {loading ? (
        <Loader
          type="Grid"
          color={Colors.BLUE}
          height={50}
          width={50}
        />
      ) : (children)}
    </>
  );
}

export default WithLoader;
