// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  footer: {
    height: 378,
    backgroundColor: 'rgba(192,199,250,.55)',
    backgroundImage: 'url(https://uploads-ssl.webflow.com/5d773ffe5b3233122e861c07/5d7e7da0500ecc5c26ce37f7_5d74692642881ea764f50d02_Group%2069.png)',
    backgroundPosition: '0px 40%, 0px 0px',
    backgroundSize: 'auto, cover',
    backgroundRepeat: 'none',
    textAlign: 'center',
    display: 'flex',
    flexFlow: 'column nowrap',
    '@media only screen and (max-width: 600px)': {
      height: 540,
    },
  },
  contentContainer: {
    width: '100%',
    flex: 1,
    display: 'flex',
    flexFlow: 'column nowrap',
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    color: Colors.DARK_GREY,
    fontSize: 25,
    margin: 0,
  },
  divider: {
    width: 146,
    margin: '16px 0',
  },
  inputContainer: {
    marginTop: 4,
    boxShadow: '0 2px 4px 0 #c5c5c5',
    display: 'flex',
    flexFlow: 'row wrap',
    '@media only screen and (min-width: 600px)': {
      width: 500,
    },
    '@media only screen and (max-width: 600px)': {
      width: '90%',
    },
    '& > *': {
      boxSizing: 'border-box',
      fontSize: 14,
    },
  },
  input: {
    height: 50,
    padding: '0 12px',
    border: 'none',
    '@media only screen and (min-width: 600px)': {
      borderRadius: '3px 0 0 3px',
      flex: 3,
    },
    '@media only screen and (max-width: 600px)': {
      borderRadius: 3,
      flex: 1,
      flexBasis: '100%',
      marginBottom: 5,
    },
  },
  submitButton: {
    flex: 1,
    height: 50,
    backgroundColor: Colors.BLUE,
    color: Colors.WHITE,
    cursor: 'pointer',
    border: 'none',
    transition: 'background-color 200ms linear',
    '&:hover': {
      backgroundColor: Colors.BLUE,
    },
    '@media only screen and (min-width: 600px)': {
      borderRadius: '0 3px 3px 0',
    },
    '@media only screen and (max-width: 600px)': {
      borderRadius: 3,
    },
  },
  bottomBarContainer: {
    width: '100%',
    padding: '27px 10px',
    backgroundColor: `${Colors.DARK_BLUE}99`,
    borderTop: `1px solid ${Colors.WHITE}1F`,
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    boxSizing: 'border-box',
    '@media only screen and (min-width: 600px)': {
      flexFlow: 'row nowrap',
    },
    '@media only screen and (max-width: 600px)': {
      flexFlow: 'column nowrap',
    },
  },
  logo: {
    height: 26,
    '@media only screen and (min-width: 600px)': {
      marginRight: 'auto',
    },
    '@media only screen and (max-width: 600px)': {
      padding: '10px 0',
    },
  },
  link: {
    fontSize: 11,
    color: `${Colors.WHITE}99`,
    textDecoration: 'none',
    transition: 'color 200ms linear',
    '&:hover': {
      color: `${Colors.WHITE}FF`,
    },
    '@media only screen and (min-width: 600px)': {
      marginLeft: 20,
    },
    '@media only screen and (max-width: 600px)': {
      height: 32,
    },
  },
  featuredAction: {
    color: Colors.MED_GREY,
    marginTop: 30,
  },
});
