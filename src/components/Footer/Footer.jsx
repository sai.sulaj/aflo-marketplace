// --------------- DEPENDENCIES -------------- //

import React, {
  useState,
} from 'react';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// --------------- MAIN --------------- //

function Footer(props) {
  const classes = useStyles(props);
  const [input, setInput] = useState('');
  const { backgroundColor = 'transparent' } = props;

  return (
    <div
      className={classes.footer}
      id="email-footer"
    >
      <div
        className={classes.contentContainer}
        style={{ backgroundColor }}
      >
        <h3 className={classes.header}>
          Get New Marketing and Sales Strategies Every Week
        </h3>
        <div className={classes.divider} />
        <form
          className={classes.inputContainer}
          action="https://aflo.us18.list-manage.com/subscribe/post"
        >
          <input type="hidden" name="u" value="ca3c5e76ff2950e68b0f0dc7a" />
          <input type="hidden" name="id" value="914d4c5499" />
          <input
            className={classes.input}
            value={input}
            placeholder="Enter your email address"
            onChange={(event) => setInput(event.target.value)}
            name="MERGE0"
            id="MERGE0"
            type="email"
          />
          <button
            type="submit"
            className={classes.submitButton}
          >
            Stay Updated
          </button>
        </form>
        <span className={classes.featuredAction}>
          Want to get featured for no charge?
          &nbsp;
          <a
            href="/contact"
            target="_blank"
            rel="noopener noreferrer"
          >
            Contact us.
          </a>
        </span>
      </div>
    </div>
  );
}

export default Footer;
