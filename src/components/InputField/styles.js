// ---------------- DEPENDENCIES --------------- //

import { makeStyles } from '@material-ui/styles';
import { Colors } from '../../constants';

// ---------------- MAIN ---------------- //

export const useStyles = makeStyles({
  inputField: {
    display: 'flex',
    flexFlow: 'row nowrap',
    alignItems: 'center',
    borderBottom: `2px solid ${Colors.MED_GREY}88`,
    transition: 'border-bottom 100ms linear',
    '&:focus-within': {
      borderBottom: `2px solid ${Colors.BLUE}`,
    },
  },
  fullWidth: {
    width: '100%',
  },
  input: {
    flex: 1,
    border: 'none',
    height: 40,
    fontSize: 16,
    padding: '0 10px',
    color: Colors.DARK_GREY,
    '&::placeholder': {
      color: `${Colors.MED_GREY}88`,
    },
    '&:focus': {
      outline: 0,
    },
  },
  icon: {
    fontSize: 25,
    transform: 'translateY(-2px)',
  },
});
