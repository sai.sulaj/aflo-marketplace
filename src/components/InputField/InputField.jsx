// --------------- DEPENDENCIES -------------- //

import React from 'react';
import classnames from 'classnames';

// --------------- LOCAL DEPENDENCIES -------------- //

// Styles.
import { useStyles } from './styles';

// --------------- MAIN --------------- //

function InputField(props) {
  const classes = useStyles(props);
  const {
    className,
    fullWidth,
    icon,
    iconColor,
    onChange,
    ...other
  } = props;

  const Icon = icon;

  function handleOnChange(event) {
    if (onChange) {
      onChange(event.target.value);
    }
  }

  return (
    <div
      className={classnames(
        classes.inputField,
        fullWidth && classes.fullWidth,
        className,
      )}
    >
      {icon && (
        <Icon
          className={classes.icon}
          style={{ color: iconColor }}
        />
      )}
      <input
        className={classes.input}
        onChange={handleOnChange}
        {...other}
      />
    </div>
  );
}

export default InputField;
